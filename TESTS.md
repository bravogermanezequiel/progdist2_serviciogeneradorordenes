# ¿Que tests se realizaron?

Se llevaron a cabo tests sobre todas las entidades y operaciones referidas a verbos API de forma unitaria. Para todo se verifica la salida y codigo de respuesta esperados.

## Orden

- Listado de ordenes: se realizo prueba cargando 2 ordenes por medio del ORM, y luego obtenerlo por medio de GET /ordenes
- Listado de ordenes con filtro por fecha: se realizo prueba cargando 2 ordenes por medio del ORM, y luego obtenerlo por medio de GET /ordenes/<int:anio>/<int:mes> y se verifica que solo se devuelva la orden correcta para la fecha
- Obtencion de orden existente: se realizo prueba cargando 2 ordenes por medio del ORM, y luego obter una a una por medio de GET /orden/{id}
- Obtencion de orden no existente: se realizo prueba obteniendo la orden con id 50 por medio de GET /orden/10
- Creacion de orden valida: se realizo prueba cargando una orden valida por medio de la API POST /orden y luego se valido por medio del ORM. Se debió mockear las funciones de libroExiste (la cual valida si un libro existe segun su id) y exchangeEnqueue la cual encola un mensaje en la cola RabbitMQ, y socioExiste (la cual valida si un socio existe segun su id).
- Creacion de orden invalida (libro inexistente): se realizo prueba cargando una orden invalida pues contiene un id de libro inexistente, se valido que la respuesta sea 400 HTTP y el resultado de salida. Se debió mockear las funciones de libroExiste (la cual valida si un libro existe segun su id) y exchangeEnqueue la cual encola un mensaje en la cola RabbitMQ, y socioExiste (la cual valida si un socio existe segun su id).
- Actualizar orden valida: se realizo la carga de una orden por medio del ORM y luego por medio de la API PUT /orden/{id} se actualizo la lista correctamente, para luego validar los datos por medio del ORM y los nuevos actualizados. Se debió mockear las funciones de libroExiste (la cual valida si un libro existe segun su id) y exchangeEnqueue la cual encola un mensaje en la cola RabbitMQ, y socioExiste (la cual valida si un socio existe segun su id).
- Creacion de orden invalida (socio inexistente): se realizo prueba cargando una orden invalida pues contiene un id de socio inexistente, se valido que la respuesta sea 400 HTTP y el resultado de salida. Se debió mockear las funciones de libroExiste (la cual valida si un libro existe segun su id) y exchangeEnqueue la cual encola un mensale en la cola RabbitMQ, y socioExiste (la cual valida si un socio existe segun su id).
- Obtencion de socio con mayor cantidad de envios: se realizo la carga de 6 ordenes con 2 socios diferentes (id=1 / id=2) de tal manera que para el socio 2, sus envios id sean el mismo (todas las ordenes se envian en el mismo envio) y para el socio 1 se dispuso de 2 ordenes con diferentes envios y una con envio nulo para verificar que esta no sea contada como un envio. Así luego se valida que el id del socio con mayor envios devuelto sea 1 y su cantidad de envios sea equivalente a 2.
