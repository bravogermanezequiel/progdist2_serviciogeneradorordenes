from app import db
from sqlalchemy.orm import relationship
from datetime import date

class Orden(db.Model):
    __tablename__ = "ordenes"
    id = db.Column(db.Integer, primary_key=True)
    envioId = db.Column(db.Integer)
    enviado = db.Column(db.Boolean)
    procesado = db.Column(db.Boolean)
    direccion = db.Column(db.String(128))
    fecha = db.Column(db.Date)
    socioId = db.Column(db.Integer)
    librosCantidad = relationship("LibrosCantidad")
    
    def __init__(self, direccion, socioId, envioId=None, enviado=False, procesado=False):
        self.envioId = envioId
        self.enviado = enviado
        self.direccion = direccion
        self.procesado = procesado
        self.fecha = date.today()
        self.socioId = socioId
        
    def toDict(self):
        obj = {c.name: getattr(self, c.name) for c in self.__table__.columns}
        obj["librosCantidad"] = [ libroCantidad.toDict() for libroCantidad in getattr(self, "librosCantidad") ]
        obj["fecha"] = obj["fecha"].strftime("%Y-%m-%d"),
        return obj

class LibrosCantidad(db.Model):
    __tablename__ = "librosCantidad"
    id = db.Column(db.Integer, primary_key=True)
    ordenId = db.Column(db.Integer, db.ForeignKey('ordenes.id'))
    libroId = db.Column(db.Integer)
    cantidad = db.Column(db.Integer)

    def __init__(self, libroId, cantidad):
        self.libroId = libroId
        self.cantidad = cantidad

    def toDict(self):
       return [getattr(self, "libroId"), getattr(self, "cantidad")]