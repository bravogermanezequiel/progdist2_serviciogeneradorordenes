from flask import request, Blueprint
from repositorio import *
from excepciones import *

HTTP_OK_CREATED = 201
HTTP_OK_REQUEST = 200
HTTP_BAD_REQUEST = 400
HTTP_NOT_FOUND = 404
HTTP_SERVICE_UNAVAILABEL = 503

ruta = Blueprint("rutas", __name__, url_prefix="")

@ruta.route("/", methods=["GET"])
def server_info():
    return jsonify(RootRepositorio().get()), HTTP_OK_REQUEST

@ruta.route("/ordenes/", methods=["GET"])
@ruta.route("/ordenes/<int:anio>/<int:mes>", methods=["GET"])
def listOrdenes(anio=None, mes=None):
    try:
        res =OrdenesRepositorio().list(anio, mes)
        return jsonify(res), HTTP_OK_REQUEST 
    except MesFueraRangoException:
        return jsonify({ "Resultado": "El mes debe encontrarse en el rango 1..12"}), HTTP_BAD_REQUEST

@ruta.route("/orden/<int:id>", methods=["GET"])
def getOrden(id):
    try:
        orden = OrdenesRepositorio().get(id)
        return jsonify(orden), HTTP_OK_REQUEST
    except NotFoundException as e:
        return jsonify({ "Resultado": "No encontrado"}), HTTP_NOT_FOUND
    
@ruta.route("/orden/", methods=["POST"])
def createOrden():
    try:
        resp = OrdenesRepositorio().create()        
        return jsonify(resp), HTTP_OK_CREATED
    except BadRequestException as e:
        return jsonify({ "Resultado": str(e) }), HTTP_BAD_REQUEST
    except ColaCaidaException:
        return jsonify({ "Resultado": "Parece que algo no funciona bien. Intente mas tarde..." }), HTTP_SERVICE_UNAVAILABEL


# No se admite actualizacion de parametro "librosCantidad"
@ruta.route("/orden/<int:id>", methods=["PUT"])
def updateOrden(id):
    try:
        resp = OrdenesRepositorio().update(id)
        return jsonify(resp), HTTP_OK_REQUEST
    except NotFoundException:
        return jsonify({ "Resultado": "No encontrado" }), HTTP_NOT_FOUND
    except BadRequestException as e:
        return jsonify({ "Resultado": str(e) }), HTTP_BAD_REQUEST

@ruta.route("/orden/socio/mayorEnvios", methods=["GET"])
def obtenerSocioMayorEnvios():
    try:
        res = OrdenesRepositorio().obtenerSocioMayorEnvios()
        return jsonify(res), HTTP_OK_REQUEST
    except EnvioNotFoundException:
        return jsonify({"Resultado": "No hay envios para ordenes cargadas"}), HTTP_NOT_FOUND

@ruta.before_request
def beforeRequest():
    log = logging.getLogger('RECEIVEREQUEST_LOGGING')
    endpoint = request.path
    method = request.method
    body = request.data.replace(b'\n', b'').decode('utf-8')
    log.info(f"[Entrante] Request. request.method={method} request.endpoint={endpoint} request.body='{body}'")

@ruta.after_request
def afterRequest(response):
    log = logging.getLogger('RECEIVEREQUEST_LOGGING')
    endpoint = request.path
    method = request.method
    data = response.get_data().replace(b'\n', b'').decode('utf-8')
    log.info(f"[Entrante] Response. response.method={method} response.endpoint={endpoint} response.data='{data}' response.status_code={response.status_code}")
    return response