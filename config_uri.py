from os import environ

URL_LIBRERIA = environ.get("URL_LIBRERIA")
HOST_AMQP = environ.get("HOST_AMQP")