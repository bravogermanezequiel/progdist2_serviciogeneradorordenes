from . import BaseTestClass
from app import db
from entidades import Orden, LibrosCantidad
from auxiliar import ConsultorLibros, ConsultorSocios, MessageQueuer
from repositorio import OrdenesRepositorio
import json
from mock import patch
from excepciones import *
from datetime import date

class TestOrdenes(BaseTestClass):
    def testRutaListarOrdenes(self):
        with self.app.app_context():
            dateStr = str(date.today())
            ordenes = [
                {
                    "direccion": "Buenaventura 1299",
                    "enviado": False,
                    "envioId": None,
                    "procesado": False,
                    "socioId": 1,
                    "libros": [
                        [1,5]
                    ]
                },
                {
                    "direccion": "Piedra negra 3212",
                    "enviado": True,
                    "envioId": None,
                    "procesado": True,
                    "socioId": 1,
                    "libros": [
                        [2,1]
                    ]
                }
            ]

            for orden in ordenes:
                ordenPr = orden.copy()
                ordenPr.pop("libros")
                ordenObj = Orden(**ordenPr)
                
                for libroId, cantidad in orden["libros"]:
                    ordenObj.librosCantidad.append(LibrosCantidad(libroId, cantidad))

                db.session.add(ordenObj)
            
            db.session.commit()
        
            res = self.client.get("/ordenes/")
            assert res.status_code == 200

            jsonData = json.loads(res.data.decode())

            for i, orden in enumerate(ordenes):
                assert jsonData["ordenes"][i]["enviado"] == orden["enviado"]
                assert jsonData["ordenes"][i]["envioId"] == orden["envioId"]
                assert jsonData["ordenes"][i]["libros"] == orden["libros"]
                assert jsonData["ordenes"][i]["fecha"] == dateStr
                assert jsonData["ordenes"][i]["procesado"] == orden["procesado"]
                assert jsonData["ordenes"][i]["socioId"] == orden["socioId"]

    def testRutaListarOrdenesFiltrado(self):
        with self.app.app_context():
            dateStr = str(date.today())
            ordenes = [
                {
                    "direccion": "Buenaventura 1299",
                    "enviado": False,
                    "envioId": None,
                    "procesado": False,
                    "socioId": 1,
                    "libros": [
                        [1,5]
                    ]
                },
                {
                    "direccion": "Piedra negra 3212",
                    "enviado": True,
                    "envioId": None,
                    "procesado": True,
                    "socioId": 2,
                    "libros": [
                        [2,1]
                    ]
                }
            ]

            fechas = [
                date(2021, 1, 1),
                date(2020, 5, 2)
            ]

            for i, orden in enumerate(ordenes):
                ordenPr = orden.copy()
                ordenPr.pop("libros")
                ordenObj = Orden(**ordenPr)
                ordenObj.fecha = fechas[i]
                
                for libroId, cantidad in orden["libros"]:
                    ordenObj.librosCantidad.append(LibrosCantidad(libroId, cantidad))

                db.session.add(ordenObj)
            
            db.session.commit()
        
            res = self.client.get("/ordenes/2021/01")
            assert res.status_code == 200

            jsonData = json.loads(res.data.decode())
            
            assert len(jsonData["ordenes"]) == 1
            assert jsonData["ordenes"][0]["enviado"] == ordenes[0]["enviado"]
            assert jsonData["ordenes"][0]["envioId"] == ordenes[0]["envioId"]
            assert jsonData["ordenes"][0]["libros"] == ordenes[0]["libros"]
            assert jsonData["ordenes"][0]["fecha"] == str(fechas[0])
            assert jsonData["ordenes"][0]["procesado"] == ordenes[0]["procesado"]
            assert jsonData["ordenes"][0]["socioId"] == ordenes[0]["socioId"]

    def testRutaObtenerOrden(self):
        with self.app.app_context():
            ordenes = [
                {
                    "direccion": "Buenaventura 1299",
                    "enviado": False,
                    "envioId": None,
                    "procesado": False,
                    "socioId": 1,
                    "librosCantidad": [
                        [1,5]
                    ]
                },
                {
                    "direccion": "Piedra negra 3212",
                    "enviado": True,
                    "envioId": None,
                    "procesado": True,
                    "socioId": 2,
                    "librosCantidad": [
                        [2,1]
                    ]
                }
            ]

            for orden in ordenes:
                ordenPr = orden.copy()
                ordenPr.pop("librosCantidad")
                ordenObj = Orden(**ordenPr)
                
                for libroId, cantidad in orden["librosCantidad"]:
                    ordenObj.librosCantidad.append(LibrosCantidad(libroId, cantidad))

                db.session.add(ordenObj)
            
            db.session.commit()

            for i, orden in enumerate(ordenes):
                res = self.client.get(f"/orden/{i+1}")
                assert res.status_code == 200

                jsonData = json.loads(res.data.decode())
                assert jsonData["enviado"] == orden["enviado"]
                assert jsonData["envioId"] == orden["envioId"]
                assert jsonData["librosCantidad"] == orden["librosCantidad"]
                assert jsonData["procesado"] == orden["procesado"]
                assert jsonData["socioId"] == orden["socioId"]

    def testRutaObtenerOrdenNoExistente(self):
        with self.app.app_context():
            res = self.client.get("/orden/50")

            assert res.status_code == 404

    @patch.object(ConsultorLibros, "libroExiste")
    @patch.object(MessageQueuer, "exchangeEnqueue")
    @patch.object(ConsultorSocios, "socioExiste")
    def testRutaCrearOrdenOk(self, mockSocioExiste, mockExchangeEnqueue, mockLibroExiste):
        mockLibroExiste.return_value = True
        mockExchangeEnqueue.return_value = 1
        mockSocioExiste.return_value = True
        with self.app.app_context():
            orden = {
                "direccion": "Buenaventura 1299",
                "socioId": 1,
                "librosCantidad": [
                    [1,5]
                ]
            }

            res = self.client.post("/orden/", json=orden)

            assert res.status_code == 201
            
            jsonData = json.loads(res.data.decode())

            ordenDB = Orden.query.get(jsonData["id"])

            socioExisteArgs, kwargs = mockSocioExiste.call_args

            assert socioExisteArgs[0] == orden["socioId"]
            assert ordenDB.socioId == orden["socioId"]
            assert ordenDB.direccion == orden["direccion"]
            assert ordenDB.librosCantidad[0].libroId == orden["librosCantidad"][0][0]
            assert ordenDB.librosCantidad[0].cantidad == orden["librosCantidad"][0][1]

    @patch.object(ConsultorLibros, "libroExiste")
    @patch.object(MessageQueuer, "exchangeEnqueue")
    @patch.object(ConsultorSocios, "socioExiste")
    def testRutaCrearOrdenLibroInexistente(self, mockSocioExiste, mockExchangeEnqueue, mockLibroExiste):
        mockLibroExiste.return_value = False
        mockExchangeEnqueue.return_value = 1
        mockSocioExiste.return_value = True
        with self.app.app_context():
            orden = {
                "direccion": "Buenaventura 1299",
                "librosCantidad": [
                    [1,5]
                ]
            }

            res = self.client.post("/orden/", json=orden)

            assert res.status_code == 400
            
            jsonData = json.loads(res.data.decode())

            assert jsonData["Resultado"] == "El libro con id 1 no existe"

    @patch.object(ConsultorLibros, "libroExiste")
    @patch.object(MessageQueuer, "exchangeEnqueue")
    @patch.object(ConsultorSocios, "socioExiste")
    def testRutaActualizarOrdenOk(self, mockSocioExiste, mockExchangeEnqueue, mockLibroExiste):
        mockLibroExiste.return_value = True
        mockExchangeEnqueue.return_value = 1
        mockSocioExiste.return_value = True
        with self.app.app_context():
            ordenes = [
                {
                    "direccion": "Buenaventura 1299",
                    "enviado": False,
                    "envioId": None,
                    "procesado": False,
                    "socioId": 1,
                    "librosCantidad": [
                        [1,5]
                    ]
                }
            ]

            for orden in ordenes:
                ordenPr = orden.copy()
                ordenPr.pop("librosCantidad")
                ordenObj = Orden(**ordenPr)
                
                for libroId, cantidad in orden["librosCantidad"]:
                    ordenObj.librosCantidad.append(LibrosCantidad(libroId, cantidad))

                db.session.add(ordenObj)
            
            db.session.commit()

            updatedOrden = {
                "direccion": "Buenaventura 1300",
                "enviado": True,
                "envioId": 1,
                "procesado": True,
                "socioId": 1,
                "librosCantidad": [
                    [1,5]
                ]
            }

            res = self.client.put("/orden/1", json=updatedOrden)
            assert res.status_code == 200

            jsonData = json.loads(res.data.decode())

            ordenDB = Orden.query.get(jsonData["id"])
            assert ordenDB.enviado == updatedOrden["enviado"]
            assert ordenDB.envioId == updatedOrden["envioId"]
            assert ordenDB.librosCantidad[0].libroId == updatedOrden["librosCantidad"][0][0]
            assert ordenDB.librosCantidad[0].cantidad == updatedOrden["librosCantidad"][0][1]
            assert ordenDB.procesado == updatedOrden["procesado"]
            assert ordenDB.direccion == updatedOrden["direccion"]
            assert ordenDB.socioId == updatedOrden["socioId"]

    @patch.object(ConsultorLibros, "libroExiste")
    @patch.object(MessageQueuer, "exchangeEnqueue")
    @patch.object(ConsultorSocios, "socioExiste")
    def testRutaCrearOrdenSocioInexistente(self, mockSocioExiste, mockExchangeEnqueue, mockLibroExiste):
        mockLibroExiste.return_value = True
        mockExchangeEnqueue.return_value = 1
        mockSocioExiste.return_value = False
        with self.app.app_context():
            orden = {
                "direccion": "Buenaventura 1299",
                "socioId": 1,
                "librosCantidad": [
                    [1,5]
                ]
            }

            res = self.client.post("/orden/", json=orden)

            assert res.status_code == 400
            
            jsonData = json.loads(res.data.decode())

            assert jsonData["Resultado"] == "El socio con id 1 no existe"

    def testRutaObtenerSocioMaxCantidadEnvios(self):
        with self.app.app_context():
            ordenes = [
                {
                    "direccion": "Buenaventura 1299",
                    "enviado": False,
                    "envioId": 1,
                    "procesado": False,
                    "socioId": 1,
                    "libros": [
                        [1,5]
                    ]
                },
                {
                    "direccion": "Buenaventura 1299",
                    "enviado": False,
                    "envioId": 2,
                    "procesado": True,
                    "socioId": 1,
                    "libros": [
                        [2,1]
                    ]
                },
                {
                    "direccion": "Piedra negra 3214",
                    "enviado": False,
                    "envioId": None,
                    "procesado": True,
                    "socioId": 1,
                    "libros": [
                        [4,1]
                    ]
                },
                {
                    "direccion": "Piedra negra 3214",
                    "enviado": False,
                    "envioId": 2,
                    "procesado": True,
                    "socioId": 2,
                    "libros": [
                        [2,1]
                    ]
                },
                {
                    "direccion": "Piedra negra 3214",
                    "enviado": False,
                    "envioId": 2,
                    "procesado": True,
                    "socioId": 2,
                    "libros": [
                        [3,1]
                    ]
                },
                {
                    "direccion": "Piedra negra 3214",
                    "enviado": False,
                    "envioId": 2,
                    "procesado": True,
                    "socioId": 2,
                    "libros": [
                        [4,1]
                    ]
                }
            ]

            for orden in ordenes:
                ordenPr = orden.copy()
                ordenPr.pop("libros")
                ordenObj = Orden(**ordenPr)
                
                for libroId, cantidad in orden["libros"]:
                    ordenObj.librosCantidad.append(LibrosCantidad(libroId, cantidad))

                db.session.add(ordenObj)
            
            db.session.commit()
        
            res = self.client.get("/orden/socio/mayorEnvios")
            assert res.status_code == 200

            jsonData = res.json

            assert jsonData["socioId"] == 1
            assert jsonData["cantidadEnvios"] == 2