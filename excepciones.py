class NotFoundException(Exception):
    pass

class BadRequestException(Exception):
    pass

class OrdenNotFoundException(Exception):
    pass

class EnvioNotFoundException(Exception):
    pass

class MesFueraRangoException(Exception):
    pass

class ColaCaidaException(Exception):
    pass