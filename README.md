# API Ordenes

## Entidades

- Orden
    1. id
    2. envioId
    3. enviado
    4. procesado
    5. direccion
    6. socioId
    7. librosCantidad
- LibrosCantidad
    1. id
    2. ordenId
    3. libroId
    4. cantidad
    5. fecha

## Rutas

- Orden
    1. Listar ordenes totales:  GET /ordenes/
    2. Listar ordenes por año y mes: GET /ordenes/{int:anio}/{int:mes}
    3. Obtener orden:   GET /orden/{int:id}
    4. Crear orden:     POST /orden/
    5. Actualizar orden:    PUT /orden/{int:id}
    6. Obtener socio con mayores envios: GET /orden/socio/mayorEnvios
