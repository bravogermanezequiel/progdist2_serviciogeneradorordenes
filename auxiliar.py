import json
import pika 
from config_uri import URL_LIBRERIA, HOST_AMQP
import logging
from lib.RequestWrapper import RequestWrapper
from excepciones import ColaCaidaException

HTTP_OK_CREATED = 201
HTTP_OK_REQUEST = 200
HTTP_BAD_REQUEST = 400
HTTP_NOT_FOUND = 404

class ConsultorLibros:
    @staticmethod
    def libroExiste(id):
        URL = f"{URL_LIBRERIA}/libro/{id}"
        r = RequestWrapper.get(URL)
        return r.status_code == HTTP_OK_REQUEST

    @staticmethod
    def obtenerVolumenLibro(id):
        URL = f"{URL_LIBRERIA}/libro/{id}"
        r = RequestWrapper.get(URL)
        
        if r.status_code != HTTP_OK_REQUEST:
            return None
        
        data = json.loads(r.text)

        return data.get("volumen")

class ConsultorSocios:
    @staticmethod
    def socioExiste(id):
        URL = f"{URL_LIBRERIA}/socio/{id}"
        r = RequestWrapper.get(URL)
        
        return r.status_code == HTTP_OK_REQUEST

class MessageQueuer:
    connection = None
    channel = None
    exchangeName = None
    log = logging.getLogger('MESSAGEQUEUER_LOGGING')

    @classmethod
    def exchangeInitialize(cls, exchangeName):
        cls.exchangeName = exchangeName
        cls.connection = pika.BlockingConnection(pika.ConnectionParameters(HOST_AMQP))
        cls.log.info(f"Conexion iniciada. host.name={HOST_AMQP}")
        cls.channel = cls.connection.channel()
        cls.channel.exchange_declare(exchange=exchangeName, exchange_type='direct')
        cls.log.info(f"Exchange declarado exchange.name={exchangeName} exchange.type=direct")

    @classmethod
    def exchangeEnqueue(cls, message, numIntento=0):
        try:
            cls.channel.basic_publish(
                exchange=cls.exchangeName,
                routing_key=cls.exchangeName,
                body=json.dumps(message)
            )
            cls.log.info(f"Mensaje publicado. exchange.name={cls.exchangeName} routing.key={cls.exchangeName} body='{json.dumps(message)}'")

        except (pika.exceptions.ConnectionClosed, pika.exceptions.StreamLostError):
            cls.log.error(f"Excepcion. Conexion perdida.")
            if numIntento >= 10:
                cls.log.error("Supero intento de conexion. Cola caida!")
                raise ColaCaidaException
            cls.exchangeInitialize(cls.exchangeName)
            cls.exchangeEnqueue(message, numIntento=numIntento+1)