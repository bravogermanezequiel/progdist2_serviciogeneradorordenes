from sqlalchemy.engine import Engine
from sqlalchemy import event
from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy
from prometheus_flask_exporter import PrometheusMetrics

db = SQLAlchemy()

'''
    Decorator de listener de evento "connect". 
    Necesario para setear las constraints respecto a las Foreign Keys para que ante una eventual excepcion por una foreign key invalida, el SQLite responda con un throw Exception.
'''
@event.listens_for(Engine, "connect")
def setSQLitePragma(dbAPIConnection, connectionRecord):
    cursor = dbAPIConnection.cursor()
    cursor.execute("PRAGMA foreign_keys=ON")
    cursor.close()
    
def initApp(config="Config"):
    from verbos import ruta

    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(f"config.{config}")
    app.register_blueprint(ruta)

    db.init_app(app)
    
    metrics = PrometheusMetrics(app)

    return app