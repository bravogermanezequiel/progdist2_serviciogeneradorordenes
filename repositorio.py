from flask import jsonify
from entidades import *
from app import db
from excepciones import *
from auxiliar import ConsultorLibros, ConsultorSocios, MessageQueuer
import logging
from datetime import date
from sqlalchemy import func
from operator import attrgetter

class RootRepositorio:
    def get(self):
        return { "Servidor": "Servicio Ordenes" }
        
class OrdenesRepositorio:
    def list(self, anio, mes):
        ordenes = Orden.query

        if anio and mes:
            if mes < 1 or mes > 12:
                raise MesFueraRangoException
            
            fechaDesde = date(anio, mes, 1)
            fechaHasta = date(anio+1 if mes == 12 else anio, 1 if mes == 12 else mes+1, 1)
            ordenes = ordenes.filter(Orden.fecha>=fechaDesde,Orden.fecha<fechaHasta)

        ordenes = ordenes.order_by(Orden.id).all()

        return {
            "ordenes": [
                {
                    "id": x.id, 
                    "envioId": x.envioId, 
                    "enviado": x.enviado,
                    "procesado": x.procesado,
                    "fecha": x.fecha.strftime("%Y-%m-%d"),
                    "socioId": x.socioId,
                    "libros": [(libro.libroId, libro.cantidad) for libro in x.librosCantidad]
                } for x in ordenes
            ]
        }
    
    def get(self, id):
        orden = Orden.query.get(id)
    
        if orden is None:
            raise NotFoundException
            
        return {
            "id": orden.id,
            "envioId": orden.envioId,
            "enviado": orden.enviado,
            "procesado": orden.procesado,
            "socioId": orden.socioId,
            "fecha": orden.fecha.strftime("%Y-%m-%d"),
            "librosCantidad": [(libro.libroId, libro.cantidad) for libro in orden.librosCantidad]
        }
    
    def create(self):
        from flask import request
        json = request.get_json()
        direccion = json.get("direccion")
        libros = json.get("librosCantidad")
        socioId = json.get("socioId")

        newOrden = Orden(direccion, socioId)

        for libroId, cantidad in libros:
            if not ConsultorLibros.libroExiste(libroId):
                raise BadRequestException(f"El libro con id {libroId} no existe")

            newOrden.librosCantidad.append(LibrosCantidad(libroId, cantidad))
        
        if not ConsultorSocios.socioExiste(socioId):
            raise BadRequestException(f"El socio con id {socioId} no existe")

        MessageQueuer.exchangeEnqueue(newOrden.toDict())
        db.session.add(newOrden)
        db.session.commit()
        logging.debug(newOrden.toDict())

        return {
            "id": newOrden.id
        }

    def update(self, id):
        orden = Orden.query.get(id)
    
        if orden is None:
            raise NotFoundException
        
        columnas = orden.__table__.columns.keys()[1:] # Se seleccionan las columnas que nos interesan, es decir todas menos "id" (0) pues no se puede o debe actualizar ya que corresponde como identificador al recurso
        if "librosCantidad" in columnas:
            columnas.remove("libros")

        if "socioId" in columnas:
            columnas.remove("socioId")

        if "fecha" in columnas:
            columnas.remove("fecha")
            
        from flask import request
        js = request.get_json()
        
        for columna in columnas:
            setattr(orden, columna, js.get(columna))
                
        db.session.add(orden)
        db.session.commit()

        return {
            "id": orden.id
        }
    
    def obtenerSocioMayorEnvios(self):
        socios = db.session.query(
            func.count(func.distinct(Orden.envioId)).label("cantidadEnvios"),
            Orden.socioId
        ).group_by(
            Orden.socioId
        ).all()

        if not len(socios):
            raise EnvioNotFoundException

        maxSocioEnvios = max(socios, key=attrgetter("cantidadEnvios"))

        return {
            "socioId": maxSocioEnvios.socioId,
            "cantidadEnvios": maxSocioEnvios.cantidadEnvios
        }
